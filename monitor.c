#include "monitor.h"

/* Auxiliares: Localizacao do Palito */
int palito_esquerda(int posicao)
{
  return posicao;
}

int palito_direita(int posicao)
{
  return (1+posicao)%num_filosofos;
}

/* Auxiliar: Impressao do status do filosofo */
void impressao()
{
    //Iterador
	int i;

	for (i=0;i<num_filosofos;i++)
    {
		if(monitor_jantar.filosofos[i] == PENSANDO)
		{
            printf(" %d -> Pensando ",i);
        }
		if(monitor_jantar.filosofos[i] == COM_FOME)
		{
            printf(" %d -> Com Fome ",i);
        }
		if(monitor_jantar.filosofos[i] == COMENDO)
		{
            printf(" %d -> Comendo ",i);
        }
	}
	printf("\n");
}

/* Função: Pegar Palito */
void pegar_palito(int identificao_filosofo)
{
    int esquerda = palito_esquerda(identificao_filosofo);	
	int direita = palito_direita(identificao_filosofo);
    
    //Garantimos a exclusão mutua, setando o mutex como lock
	pthread_mutex_lock(&monitor_jantar.exclusividade);
    
        //Identificamos que o filósofo quer pegar o palito -> Está com Fome
	    monitor_jantar.filosofos[identificao_filosofo] = COM_FOME;
    
        //Imprimimos os estados dos Filosofos
        impressao();
        
        //Enquanto os palitos ao lado do filósofo estiverem em uso temos que fazer a thread do filosofo esperar
        while(monitor_jantar.palitos[direita] == USO || monitor_jantar.palitos[esquerda] == USO)
        {
            //Esperamos que a condição seja satisfeita, fazendo a thread de exclusividade esperar
            pthread_cond_wait(&monitor_jantar.condicao_filosofo[identificao_filosofo], &monitor_jantar.exclusividade);
        }
            
        
        //Setamos os palitos como em uso
        monitor_jantar.palitos[esquerda] = USO;
        monitor_jantar.palitos[direita] = USO;

        /* Depois de sair do while, sabemos que os palitos foram liberados, e o filosofo pode comer */
        monitor_jantar.filosofos[identificao_filosofo] = COMENDO;   //Mudamos seu status

        //Imprimimos novamente os estados dos Filosofos
        impressao();

    //Desbloqueio do mutex, pois a operação terminou
    pthread_mutex_unlock(&monitor_jantar.exclusividade);

}

/* Função Soltar Palitos */
void soltar_palito(int identificao_filosofo)
{
	int esquerda = palito_esquerda(identificao_filosofo);	
	int direita = palito_direita(identificao_filosofo);

    //Garantimos a exclusão mutua, setando o mutex como lock
	pthread_mutex_lock(&monitor_jantar.exclusividade);

        //O filósofo solta o palito, e eles ficam livres
        monitor_jantar.palitos[esquerda] = LIVRE;
        monitor_jantar.palitos[direita]	= LIVRE;
        
        //Filosofo sai do estado de comendo e volta a pensar
        monitor_jantar.filosofos[identificao_filosofo] = PENSANDO;

        //Impressao do estado do filosofo	
        impressao();
        
        /*Acordando os filosofos da esquerda e direita, mandando o sinal*/
        pthread_cond_signal(&monitor_jantar.condicao_filosofo[esquerda]);
        pthread_cond_signal(&monitor_jantar.condicao_filosofo[direita]);

    //Desbloqueio do mutex, pois a operação terminou
	pthread_mutex_unlock(&monitor_jantar.exclusividade);

}

void comer()
{
	sleep(TEMPO_COMER);	
}

void pensar()
{
	sleep(TEMPO_PENSAR);	
}

/* Inicializacao do Monitor */
int iniciar_monitor()
{
    //Iteradores
    int i;

    //Criando Mutexes
    int criacao_excl=pthread_mutex_init(&monitor_jantar.exclusividade, NULL);

    //Iniciando Filosofos
	for(i=0;i<num_filosofos;i++)
    {
        monitor_jantar.filosofos[i] = PENSANDO;
	}
    //Iniciando Palitos
	for(i=0;i<num_palitos;i++)
    {
        monitor_jantar.palitos[i] = LIVRE;
	}

    //Verifica se a criacão foi bem sucedida e retorna a funcao
    if(criacao_excl==0)
    {
      return 0;
    }
    else
    {
      return 1;
    }
}

void acao_filosofo(int identificao_filosofo)
{
	while(1)
    {
        //Pensa por um tempo pré definido
        pensar();

        //Filosofo faz o pedido do palito
		pegar_palito(identificao_filosofo);
        //Come por um tempo pré definido
		comer();

        //Solta o palito
		soltar_palito(identificao_filosofo);
	}

	pthread_exit(0);
}

/* Destroi o Monitor */
void destruir_monitor()
{
    //Destroi o Mutex
    pthread_mutex_destroy(&monitor_jantar.exclusividade);
}