UTIL= monitor.c
MAIN=jantar.c
BINARY=jantarfilosofos

all:
	gcc -Wall $(MAIN) $(UTIL) -o $(BINARY)

run:
	./$(BINARY)

debug:
	gcc -DDEBUG -Wall $(MAIN) $(UTIL) -o $(BINARY)

clean:
	@rm *.o