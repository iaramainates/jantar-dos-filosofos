/**
 *  Construção de um Monitor no formato de biblioteca para programas em C
 * 
 *  Inspirado no código de Brian Anderson <Github da Disciplina>
 *  e de Rodrigo Freitas leite e Felipe Soares de Paula < https://github.com/UfrgsProjects/jantar-filosofos >
 **/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

//Definicao de tempos de pensamento e de momento de comer em segundos
#define TEMPO_PENSAR 3
#define TEMPO_COMER 3

//Definicao de Qtd. de Filosofos
#define num_filosofos 5
//Definicao de Qtd. de Palitos
#define num_palitos 5

//Definicão do uso dos palitos
#define USO 1
#define LIVRE 0

//Dados do Monitor
struct monitor 
{
    //Definicao de um array de filosofos e seus estados
    enum estados{PENSANDO, COM_FOME, COMENDO} filosofos[num_filosofos];
    //Threads de Filosofos
    pthread_t threads_filosofos[num_filosofos];

    //Definicao de um array de Palitos
    int palitos[num_palitos];


    /** Variável de Condição de Thread
     *  - Permite colocar threads para dormir até que uma condição aconteça
     **/
    pthread_cond_t condicao_filosofo[num_filosofos];
    //Mutexes que garantem exclusividade e que um filosofo não morra de fome, respectivamente
    pthread_mutex_t exclusividade;
};

//Nosso monitor a ser utilizado
struct monitor monitor_jantar; 

/* Inicializacao do Monitor */
int iniciar_monitor();

/* Faz as tarefas do Monitor */
void acao_filosofo(int identificao_filosofo);

/* Destroi o Monitor */
void destruir_monitor();


/* -------------------------------------------------------------------------------------------------------------------- */

/** Lógica do Problema
 * 
 *  - Filósofos são threads
 *      - Filósofos querem acesso a 2 palitos para comer
 *      - Podem estar em 3 estados:
 *          ~ Pensando: Sem possui e nem solicitar nenhum garfo
 *          ~ Comendo: Está em posse de 2 palito
 *          ~ Com Fome: Querendo comer, precisando de 2 palito
 *      - Podem executar duas ações
 *          ~ Pegar Garfo: Verificar se há palito disponíveis
 *              -> Se estiver, aloca, muda de estado para comendo
 *              -> Senao, bloqueia e espera até que os garfos estejam disponíveis
 *          ~ Soltar Garfo: Liberar os palito e avisar os outros filósofos
 *      - Logo, um filósofo deveria
 *          1. Pensar
 *          2. Pegar Palito
 *          3. Comer
 *          4. Soltar Palito
 **/
/* -------------------------------------------------------------------------------------------------------------------- */