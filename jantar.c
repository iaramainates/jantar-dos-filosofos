/** 
 *  Projeto feito por Iara Duarte Mainates (nUSP 11816143)
 *  Disciplina de Sistemas Operacionais (SSC0541)
 * 
 *  Philosophers Dining - Using Monitors
 *  Jantar dos Filosofos - Usando Monitores
 * 
 *  Solução utilizando o repositório da disciplina < https://github.com/vbonato/OS/blob/master/monitor.c >
 *  e outras fontes, como:
 *      - Jantar com Semáforos
 *      < https://www.geeksforgeeks.org/dining-philosopher-problem-using-semaphores/ >
 *      - Jantar com Monitores
 *      < https://www.geeksforgeeks.org/dining-philosophers-solution-using-monitors/ >
 *      - PThreads e Cond
 *      < https://www.dcc.fc.up.pt/~ricroc/aulas/0708/ppd/apontamentos/pthreads.pdf >
 **/

#include <stdio.h>
#include "monitor.h"

int main() 
{
    //Iteradores
    int i;

    if(iniciar_monitor()==0)
	{
        printf("Inicio do Jantar!\n");

        for	(i=0;i<num_filosofos;i++)
        {
            //Inicia a variavel de condicao e a thread de filosofos iterativamente
            pthread_cond_init(&monitor_jantar.condicao_filosofo[i], NULL);
            pthread_create(&monitor_jantar.threads_filosofos[i], NULL, acao_filosofo, (i)); 
            //É iniciada para a funcao de ação do monitor
        }
    }
    else
    {
        printf("Não foi possível iniciar o monitor!\n");
    }
	
	pthread_exit(0);
    destruir_monitor();

    return 0;
}